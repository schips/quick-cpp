# Quick Cpp

快速地创建一个开箱即用的CPP程序。

环境要求：

- camke v3.1 +
- gcc 7      +

## 目录说明

`core/3rd/`：存放外部的第三方库


`core/`：是针对第三方库的封装实现

`example/` ：针对不同场景提供可用的demo；此目录不参与编译。

## 编译

执行`./build.sh`即可

如果想指定编译结果（输出文件名、输出目录），请修改：

- `OUTPUT_APPNAME`代表输出文件名

- `OUTPUT_DIRNAME`代表输出目录，建议指定绝对路径；否则是以编译所在路径作为主目录


## 修改编译选项

修改`CMakeModules.cmake`文件

### 编译器选项

`add_c_flag`：额外的C编译选项

`add_c_flag`：额外的CXX编译选项

### 链接新的编译库

`add_ld_flag` :额外的链接选项

`add_lib_for_link` : 额外的链接库

`add_lib_path_for_link` : 额外的链接路径

### 编译时宏定义

`add_define` : 额外的宏定义

### 指定工具链

`CROSS_COMPILE`：交叉编译工具链，例如`arm-linux-`

### 添加源码

可以使用`add_module_src`和`add_module_head`来分别添加源文件路径和头文件路径。

但建议在源码目录下实现`module.cmake`再使用`add_module_src`和`add_module_head`以保证模块的整洁性。

