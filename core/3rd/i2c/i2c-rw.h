#ifndef __I2C_RW_H__
#define __I2C_RW_H__


/********************************** for dev api **********************************/
// 对于I2C单一设备的访问，通过 dev api 操作更好。
// I2C设备信息
struct i2c_dev_info
{
    int i2c_bus;
    int i2c_fd;
    unsigned int dev_addr;
    int is_init_done;
};

/*
 * 描述  : 初始化I2C设备
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 * 返回值: 成功返回0，失败返回-1
*/
int i2c_dev_init(struct i2c_dev_info *dev);
/*
 * 描述  : 打开I2C设备
 * 参数  :
 *     i2c_bus : I2C总线号
 *     quiet  : 是(0)否在报错时提供更多信息
 * 返回值: 成功返回0，失败返回-1
*/
int i2c_dev_open(struct i2c_dev_info *dev, int i2c_bus, unsigned int dev_addr);
/*
 * 描述  : 关闭I2C设备
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_deinit(struct i2c_dev_info *dev);


/*
 * 描述  : 写I2C设备寄存器值（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_addr8_value8(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char reg_value);
/*
 * 描述  : 读I2C设备寄存器值（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_read_addr8_value8(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char *reg_value);
/*
 * 描述  : 更新写入8位寄存器中的8位值（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     mask     : 写入值的有效位，置1则有效
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_update_addr8_value8(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char mask, unsigned char reg_value);

/*
 * 描述  : 写I2C设备寄存器值（8位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_addr8_value16(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned short reg_value);
/*
 * 描述  : 读I2C设备寄存器值（8位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_read_addr8_value16(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned short *reg_value);
/*
 * 描述  : 更新写入8位寄存器中的16位值（8位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     mask     : 写入值的有效位，置1则有效
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_update_addr8_value16(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned short mask, unsigned short reg_value);

/*
 * 描述  : 写I2C设备寄存器值（16位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_addr16_value8(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned char reg_value);
/*
 * 描述  : 读I2C设备寄存器值（16位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
unsigned char i2c_dev_read_addr16_value8(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned char *reg_value);
/*
 * 描述  : 更新写入16位寄存器中的8位值（16位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     mask     : 写入值的有效位，置1则有效
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_update_addr16_value8(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned char mask, unsigned char reg_value);

/*
 * 描述  : 写I2C设备寄存器值（16位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_addr16_value16(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned short reg_value);
/*
 * 描述  : 读I2C设备寄存器值（16位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
unsigned short i2c_dev_read_addr16_value16(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned short *reg_value);
/*
 * 描述  : 更新I2C设备寄存器值（16位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 *     mask     : 写入值的有效位，置1则有效
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_update_addr16_value16(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned short mask, unsigned short reg_value);


/*
 * 描述  : 写I2C设备寄存器（8位寄存器0位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 寄存器地址
 * 返回值: 成功返回0，失败返回ERRNO
 * 注意：一般用不到这个函数，这个函数只在访问特殊的器件才会用到（例如I2C switcher）
*/
int i2c_dev_write_addr8_value0(struct i2c_dev_info *dev, unsigned char reg_addr);


/*
 * 描述  : 连续写I2C设备寄存器（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 起始寄存器地址
 *     values : 值
 *     value_cnt : 值个数
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_write_addr8_valuex(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char *values, unsigned int value_cnt);
/*
 * 描述  : 连续读I2C设备寄存器（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C设备信息的实体
 *     reg_addr : 起始寄存器地址
 *     values : 值
 *     value_cnt : 值个数
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_dev_read_addr8_valuex(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char *values, unsigned int read_cnt);



/********************************** for bus api **********************************/
// 对于I2C多个设备的访问，通过 bus api 操作更好，可以灵活指定设备地址

/*
 * 描述  : 打开I2C总线
 * 参数  :
 *     i2c_bus : I2C总线号
 * 返回值: 成功返回fd，失败返回ERRNO
 * 注意  ：无
*/
int i2c_bus_open(int i2c_bus);

/*
 * 描述  : 关闭I2C总线
 * 参数  :
 *     fd : open 成功时得到的fd
 * 返回值: 成功返回fd，失败返回ERRNO
 * 注意  ：无
*/
int i2c_bus_close(int fd);


/*
 * 描述  : 写I2C总线寄存器值（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_addr8_value8(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char reg_value);
/*
 * 描述  : 读I2C总线寄存器值（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_read_addr8_value8(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_value);
/*
 * 描述  : 更新I2C总线寄存器值（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     mask     : 写入值的有效位，置1则有效
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_update_addr8_value8(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char mask, unsigned char reg_value);

/*
 * 描述  : 写I2C总线寄存器值（8位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_addr8_value16(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned short reg_value);
/*
 * 描述  : 读I2C总线寄存器值（8位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
unsigned char i2c_bus_read_addr8_value16(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned short *reg_value);
/*
 * 描述  : 更新I2C总线寄存器值（8位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     mask     : 写入值的有效位，置1则有效
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_update_addr8_value16(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned short mask, unsigned short reg_value);

/*
 * 描述  : 写I2C总线寄存器值（16位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_addr16_value8(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned char reg_value);
/*
 * 描述  : 读I2C总线寄存器值（16位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
unsigned char i2c_bus_read_addr16_value8(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned char *reg_value);
/*
 * 描述  : 更新I2C总线寄存器值（16位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     mask     : 写入值的有效位，置1则有效
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_update_addr16_value8(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned char mask, unsigned char reg_value);


/*
 * 描述  : 写I2C总线寄存器值（16位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_addr16_value16(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned short reg_value);
/*
 * 描述  : 读I2C总线寄存器值（16位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
unsigned short i2c_bus_read_addr16_value16(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned short *reg_value);
/*
 * 描述  : 更新I2C总线寄存器值（16位寄存器16位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 *     mask     : 写入值的有效位，置1则有效
 *     reg_value: 寄存器值
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_update_addr16_value16(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned short mask, unsigned short reg_value);

/*
 * 描述  : 写I2C总线寄存器（8位寄存器0位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 寄存器地址
 * 返回值: 成功返回0，失败返回ERRNO
 * 注意：一般用不到这个函数，这个函数只在访问特殊的器件才会用到（例如I2C switcher）
*/
int i2c_bus_write_addr8_value0(int fd, unsigned char dev_addr, unsigned char reg_addr);


/*
 * 描述  : 连续写I2C总线寄存器（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 起始寄存器地址
 *     values : 值
 *     value_cnt : 值个数
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_write_addr8_valuex(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char *values, unsigned int value_cnt);
/*
 * 描述  : 连续读I2C总线寄存器（8位寄存器8位寄存器值）
 * 参数  :
 *     dev : 储存I2C总线信息的实体
 *     reg_addr : 起始寄存器地址
 *     values : 值
 *     value_cnt : 值个数
 * 返回值: 成功返回0，失败返回ERRNO
*/
int i2c_bus_read_addr8_valuex(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char *values, unsigned int read_cnt);


#endif /* __I2C_RW_H__ */
