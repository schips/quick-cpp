#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
//#include "i2cbusses.h"
//#include "util.h"
//#include "../version.h"
#include "i2c-rw.h"

#define   I2C_GET_BITS(x, end, start)   ( (x & ~(~(0U)<<((end)-(start) + 1))<<(start)) >> (start) )

#define MISSING_FUNC_FMT   "Error: Adapter does not have %s capability\n"

int max_try = 1;

// 检查I2C是否具备功能
int i2c_check_funcs(int fd)
{
    unsigned long funcs;

    /* check adapter functionality */
    if (ioctl(fd, I2C_FUNCS, &funcs) < 0) {
        fprintf(stderr, "Error: Could not get the adapter "
            "functionality matrix: %s\n", strerror(errno));
        return -1;
    }

    if (!(funcs & I2C_FUNC_I2C)) {
        fprintf(stderr, MISSING_FUNC_FMT, "I2C transfers");
        return -1;
    }

    return 0;
}

// sleep in millisecond(ms, 1 s = 1000 ms).
static inline int i2c_msleep(unsigned int ms)
{
#if 0
    int ret;
    struct timeval tval;
    tval.tv_sec  =  ms/1000;
    tval.tv_usec = (ms*1000)%1000000;
    ret = select(0,NULL,NULL,NULL,&tval);
    return ret;
#else
    return 0;
#endif
}

// 写入8位寄存器中的8位值
int i2c_bus_write_addr8_value8(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char reg_value)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf[2];
    int nmsgs = 0, ret;

    //printf("i2ctransfer -f -y 11 w2@0x%x 0x%x 0x%x\n", dev_addr, reg_addr, reg_value);

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 2;
    msgs[0].buf = buf;
    buf[0] = reg_addr;
    buf[1] = reg_value;

    rdwr.msgs = msgs;
    rdwr.nmsgs = 1;
    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    return ret;
}

// 读取8位寄存器中的8位值
int i2c_bus_read_addr8_value8(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char *reg_value)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf_0[1];
    int nmsgs = 0, ret;

    if(reg_value == NULL)
    {
        return -1;
    }

    //printf("i2ctransfer -f -y 11 w1@0x%x 0x%x r1\n", dev_addr, reg_addr);

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = buf_0;
    buf_0[0] = reg_addr;

    msgs[1].addr = dev_addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = reg_value;

    rdwr.msgs = msgs;
    rdwr.nmsgs = 2;
    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    //printf("ret %d\n", ret);
#if 0
    // for read
    unsigned i;
    __u16 j;
    //int flags = PRINT_READ_BUF | (0 ? PRINT_HEADER | PRINT_WRITE_BUF : 0);
    for (i = 0; i < ret; i++) {
        int read = msgs[i].flags & I2C_M_RD;
        int recv_len = msgs[i].flags & I2C_M_RECV_LEN;
        //int print_buf = (read && (flags & PRINT_READ_BUF)) ||
        //      (!read && (flags & PRINT_WRITE_BUF));
        __u16 len = recv_len ? msgs[i].buf[0] + 1 : msgs[i].len;

/*
        if (flags & PRINT_HEADER) {
            printf("msg %u: addr 0x%02x, %s, len ",
                i, msgs[i].addr, read ? "read" : "write");
            if (!recv_len || flags & PRINT_READ_BUF)
                printf("%u", len);
            else
                printf("TBD");
        }
*/

        if (len && read) {
            //if (flags & PRINT_HEADER) printf(", buf ");
            for (j = 0; j < len - 1; j++)
                printf("0x%02x ", msgs[i].buf[j]);
            /* Print final byte with newline */
            printf("0x%02x\n", msgs[i].buf[j]);
        }
    }
#else
    //printf("0x%02x\n", msgs[1].buf[0]);
#endif
    return ret;
}

// 更新写入8位寄存器中的8位值
int i2c_bus_write_update_addr8_value8(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char mask, unsigned char reg_value)
{
    int ret;
    unsigned char tmp, orig;

    ret = i2c_bus_read_addr8_value8(fd, dev_addr, reg_addr, &orig);
    if (ret != 0)
    {
        return ret;
    }

    tmp = orig & ~mask;
    tmp |= reg_value & mask;

#if 0
    if (force_write || (tmp != orig)) {
        ret = i2c_bus_write_addr8_value8(fd, dev_addr, reg_addr, tmp);
        if (ret == 0 && change)
            *change = true;
    }
#else
    return i2c_bus_write_addr8_value8(fd, dev_addr, reg_addr, tmp);
#endif
}

// 写入8位寄存器中16位的值
int i2c_bus_write_addr8_value16(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned short reg_value)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf[3];
    int nmsgs = 0, ret = 0;

    //printf("i2ctransfer -f -y 11 w3@0x%x 0x%x 0x%x  0x%x\n", dev_addr, I2C_GET_BITS(reg_addr, 15, 8), I2C_GET_BITS(reg_addr,  7, 0), reg_value);

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 1 + 2;
    msgs[0].buf = buf;

    buf[0] = reg_addr;
    buf[1] = I2C_GET_BITS(reg_value, 15, 8);
    buf[2] = I2C_GET_BITS(reg_value,  7, 0);


    rdwr.msgs = msgs;
    rdwr.nmsgs = 1;

    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            //fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    return ret;
}

// 读取8位寄存器中16位的值
unsigned char i2c_bus_read_addr8_value16(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned short *reg_value)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf_0[1];
    int nmsgs = 0, ret = 0;
    int retry = 0;

    if(reg_value == NULL)
    {
        return -1;
    }

    /*
    printf("i2ctransfer -f -y 11 w3@0x%x 0x%x 0x%x  0x%x\n", dev_addr,
           I2C_GET_BITS(reg_addr, 15, 8), I2C_GET_BITS(reg_addr,  7, 0),
           reg_addr);
    */

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = buf_0;
    buf_0[0] = reg_addr;

    msgs[1].addr = dev_addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 2;
    msgs[1].buf = (unsigned char *)reg_value;

    rdwr.msgs = msgs;
    rdwr.nmsgs = 2;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    //printf("ret %d\n", ret);
#if 0
    // for read
    unsigned i;
    __u16 j;
    //int flags = PRINT_READ_BUF | (0 ? PRINT_HEADER | PRINT_WRITE_BUF : 0);
    //printf("Printf flags is 0x%x, %x, %x\n", flags, PRINT_HEADER, PRINT_WRITE_BUF, PRINT_READ_BUF);
    for (i = 0; i < ret; i++) {
        int read = msgs[i].flags & I2C_M_RD;
        int recv_len = msgs[i].flags & I2C_M_RECV_LEN;
        //int print_buf = (read && (flags & PRINT_READ_BUF)) ||
        //        (!read && (flags & PRINT_WRITE_BUF));
        __u16 len = recv_len ? msgs[i].buf[0] + 1 : msgs[i].len;

        if (len && read) {
            //if (flags & PRINT_HEADER) printf(", buf ");
            for (j = 0; j < len - 1; j++)
                printf("0x%02x ", msgs[i].buf[j]);
            /* Print final byte with newline */
            printf("0x%02x\n", msgs[i].buf[j]);
        }
    }
#else
    //printf("0x%02x\n", msgs[1].buf[0]);
#endif
    return ret;
}

// 更新写入8位寄存器中的16位值
int i2c_bus_write_update_addr8_value16(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned short mask, unsigned short reg_value)
{
    int ret;
    unsigned short tmp, orig;

    ret = i2c_bus_read_addr8_value16(fd, dev_addr, reg_addr, &orig);
    if (ret != 0)
    {
        printf("Error when update, read failed!\n");
        return ret;
    }

    printf("Read Reg [0x%x], val [0x%x]\n", reg_addr, orig);
    tmp = orig & ~mask;
    tmp |= reg_value & mask;
    printf("Mask Reg [0x%x], val [0x%x]\n", reg_addr, tmp);

#if 0
    if (force_write || (tmp != orig)) {
        ret = i2c_bus_write_addr8_value16(fd, dev_addr, reg_addr, tmp);
        if (ret == 0 && change)
            *change = true;
    }
#else
    return i2c_bus_write_addr8_value16(fd, dev_addr, reg_addr, tmp);
#endif
}


// 写入16位寄存器中8位的值
int i2c_bus_write_addr16_value8(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned char reg_value)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf[3];
    int nmsgs = 0, ret = 0;

    //printf("i2ctransfer -f -y 11 w3@0x%x 0x%x 0x%x  0x%x\n", dev_addr, I2C_GET_BITS(reg_addr, 15, 8), I2C_GET_BITS(reg_addr,  7, 0), reg_value);

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 2 + 1;
    msgs[0].buf = buf;
    buf[0] = I2C_GET_BITS(reg_addr, 15, 8);
    buf[1] = I2C_GET_BITS(reg_addr,  7, 0);
    buf[2] = reg_value;

    rdwr.msgs = msgs;
    rdwr.nmsgs = 1;

    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            //fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    return ret;
}

// 读取16位寄存器中8位的值
unsigned char i2c_bus_read_addr16_value8(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned char *reg_value)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf_0[2];
    int nmsgs = 0, ret = 0;
    int retry = 0;

    if(reg_value == NULL)
    {
        return -1;
    }

    /*
    printf("i2ctransfer -f -y 11 w3@0x%x 0x%x 0x%x  0x%x\n", dev_addr,
           I2C_GET_BITS(reg_addr, 15, 8), I2C_GET_BITS(reg_addr,  7, 0),
           reg_addr);
    */

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 2;
    msgs[0].buf = buf_0;
    buf_0[0] = I2C_GET_BITS(reg_addr, 15, 8);
    buf_0[1] = I2C_GET_BITS(reg_addr,  7, 0);

    msgs[1].addr = dev_addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 1;
    msgs[1].buf = reg_value;

    rdwr.msgs = msgs;
    rdwr.nmsgs = 2;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    //printf("ret %d\n", ret);
#if 0
    // for read
    unsigned i;
    __u16 j;
    //int flags = PRINT_READ_BUF | (0 ? PRINT_HEADER | PRINT_WRITE_BUF : 0);
    //printf("Printf flags is 0x%x, %x, %x\n", flags, PRINT_HEADER, PRINT_WRITE_BUF, PRINT_READ_BUF);
    for (i = 0; i < ret; i++) {
        int read = msgs[i].flags & I2C_M_RD;
        int recv_len = msgs[i].flags & I2C_M_RECV_LEN;
        //int print_buf = (read && (flags & PRINT_READ_BUF)) ||
        //        (!read && (flags & PRINT_WRITE_BUF));
        __u16 len = recv_len ? msgs[i].buf[0] + 1 : msgs[i].len;

        if (len && read) {
            //if (flags & PRINT_HEADER) printf(", buf ");
            for (j = 0; j < len - 1; j++)
                printf("0x%02x ", msgs[i].buf[j]);
            /* Print final byte with newline */
            printf("0x%02x\n", msgs[i].buf[j]);
        }
    }
#else
    //printf("0x%02x\n", msgs[1].buf[0]);
#endif
    return ret;
}

// 更新写入16位寄存器中的8位值
int i2c_bus_write_update_addr16_value8(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned char mask, unsigned char reg_value)
{
    int ret;
    unsigned char tmp, orig;

    ret = i2c_bus_read_addr16_value8(fd, dev_addr, reg_addr, &orig);
    if (ret != 0)
    {
        return ret;
    }

    tmp = orig & ~mask;
    tmp |= reg_value & mask;

#if 0
    if (force_write || (tmp != orig)) {
        ret = i2c_bus_write_addr16_value8(fd, dev_addr, reg_addr, tmp);
        if (ret == 0 && change)
            *change = true;
    }
#else
    return i2c_bus_write_addr16_value8(fd, dev_addr, reg_addr, tmp);
#endif
}

// 写入16位寄存器16位值
int i2c_bus_write_addr16_value16(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned short reg_value)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf[4];
    int nmsgs = 0, ret = 0;

    /*
    printf("i2ctransfer -f -y 11 w3@0x%x 0x%x 0x%x  0x%x 0x%x\n", dev_addr,
           I2C_GET_BITS(reg_addr, 15, 8), I2C_GET_BITS(reg_addr,  7, 0),
           I2C_GET_BITS(reg_value, 15, 8), I2C_GET_BITS(reg_value,  7, 0));
    */

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 2 + 2;
    msgs[0].buf = buf;
    buf[0] = I2C_GET_BITS(reg_addr, 15, 8);
    buf[1] = I2C_GET_BITS(reg_addr,  7, 0);
    buf[2] = I2C_GET_BITS(reg_value, 15, 8);
    buf[3] = I2C_GET_BITS(reg_value,  7, 0);

    rdwr.msgs = msgs;
    rdwr.nmsgs = 1;
    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    return ret;
}

// 读取16位寄存器中的16位值（端序未确定）
unsigned short i2c_bus_read_addr16_value16(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned short *reg_value)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf_0[2];
    int nmsgs = 0, ret = 0;

    if(reg_value == NULL)
    {
        return -1;
    }

    /*
    printf("i2ctransfer -f -y 11 w3@0x%x 0x%x 0x%x  0x%x\n", dev_addr,
           I2C_GET_BITS(reg_addr, 15, 8), I2C_GET_BITS(reg_addr,  7, 0),
           reg_addr);
    */

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 2;
    msgs[0].buf = buf_0;
    buf_0[0] = I2C_GET_BITS(reg_addr, 15, 8);
    buf_0[1] = I2C_GET_BITS(reg_addr,  7, 0);

    msgs[1].addr = dev_addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = 2;
    msgs[1].buf = (unsigned char *)reg_value;

    rdwr.msgs = msgs;
    rdwr.nmsgs = 2;
    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    //printf("ret %d\n", ret);
#if 0
    // for read
    unsigned i;
    __u16 j;
    //int flags = PRINT_READ_BUF | (0 ? PRINT_HEADER | PRINT_WRITE_BUF : 0);
    //printf("Printf flags is 0x%x, %x, %x\n", flags, PRINT_HEADER, PRINT_WRITE_BUF, PRINT_READ_BUF);
    for (i = 0; i < ret; i++) {
        int read = msgs[i].flags & I2C_M_RD;
        int recv_len = msgs[i].flags & I2C_M_RECV_LEN;
        //int print_buf = (read && (flags & PRINT_READ_BUF)) ||
        //        (!read && (flags & PRINT_WRITE_BUF));
        __u16 len = recv_len ? msgs[i].buf[0] + 1 : msgs[i].len;

        if (len && read) {
            //if (flags & PRINT_HEADER) printf(", buf ");
            for (j = 0; j < len - 1; j++)
                printf("0x%02x ", msgs[i].buf[j]);
            /* Print final byte with newline */
            printf("0x%02x\n", msgs[i].buf[j]);
        }
    }
#else
    //printf("0x%02x%02x\n", msgs[1].buf[0], msgs[1].buf[1]);
#endif
    return msgs[1].buf[0] | (msgs[1].buf[1] << 8);
}

// 更新写入16位寄存器中的16位值
int i2c_bus_write_update_addr16_value16(int fd, unsigned char dev_addr, unsigned short reg_addr, unsigned short mask, unsigned short reg_value)
{
    int ret;
    unsigned short tmp, orig;

    ret = i2c_bus_read_addr16_value16(fd, dev_addr, reg_addr, &orig);
    if (ret != 0)
        return ret;

    tmp = orig & ~mask;
    tmp |= reg_value & mask;

#if 0
    if (force_write || (tmp != orig)) {
        ret = i2c_bus_write_addr16_value16(fd, dev_addr, reg_addr, tmp);
        if (ret == 0 && change)
            *change = true;
    }
#else
    return i2c_bus_write_addr16_value16(fd, dev_addr, reg_addr, tmp);
#endif
}


// I2C总线连续收
int i2c_bus_read_addr8_valuex(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char *values, unsigned int read_cnt)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf_0[1];
    //unsigned char buf_1[1];
    int nmsgs = 0, ret = 0;
    // for read
    unsigned i;
    __u16 j;
    //int flags = PRINT_READ_BUF | (0 ? PRINT_HEADER | PRINT_WRITE_BUF : 0);

    /*
    printf("i2ctransfer -f -y 11 w3@0x%x 0x%x 0x%x  0x%x\n", dev_addr,
           I2C_GET_BITS(reg_addr, 15, 8), I2C_GET_BITS(reg_addr,  7, 0),
           reg_addr);
    */

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = buf_0;
    buf_0[0] = reg_addr;

    msgs[1].addr = dev_addr;
    msgs[1].flags = I2C_M_RD;
    msgs[1].len = read_cnt;
    msgs[1].buf = values;

    rdwr.msgs = msgs;
    rdwr.nmsgs = 2;
    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
    //printf("ret %d\n", ret);
    for (i = 0; i < ret; i++) {
        int recv_len = msgs[i].flags & I2C_M_RECV_LEN;
        __u16 len = recv_len ? msgs[i].buf[0] + 1 : msgs[i].len;

        if (len) {
            for (j = 0; j < len - 1; j++)
                printf("0x%02x ", msgs[i].buf[j]);
            /* Print final byte with newline */
            printf("0x%02x\n", msgs[i].buf[j]);
        }
    }
    //return msgs[1].buf[0];
    return ret;
}

// for i2c-switcher
int i2c_bus_write_addr8_value0(int fd, unsigned char dev_addr, unsigned char reg_addr)
{
    struct i2c_msg msgs[I2C_RDRW_IOCTL_MAX_MSGS]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    unsigned char buf[1];
    int nmsgs = 0, ret = 0;

    //printf("i2ctransfer -f -y 11 w1@0x%x 0x%x\n", dev_addr, reg_addr);

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = 1;
    msgs[0].buf = buf;
    buf[0] = reg_addr;

    rdwr.msgs = msgs;
    rdwr.nmsgs = 1;
    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }

    return ret;
}

// 针对i2c总线 连续写
int i2c_bus_write_addr8_valuex(int fd, unsigned char dev_addr, unsigned char reg_addr, unsigned char *values, unsigned int value_cnt)
{
    struct i2c_msg msgs[1]; // 42 is I2C_RDRW_IOCTL_MAX_MSGS
    struct i2c_rdwr_ioctl_data rdwr;
    int i;
    unsigned char *buf = NULL;
    int nmsgs = 0, ret = 0;
    unsigned long len;
    if(values == NULL)
    {
        return -1;
    }

    len = 1 + value_cnt; // 第一个是寄存器地址
    if(len > 0xffff)
    {
        return -1;
    }
    if(len)
    {
        buf = malloc(len);
    }
    if(buf == NULL)
    {
        return -1;
    }

    msgs[0].addr = dev_addr;
    msgs[0].flags = 0;
    msgs[0].len = len;
    msgs[0].buf = buf;
    buf[0] = reg_addr;
    for(i = 0; i < len; i++)
    {
        buf[i + 1] = values[i];
    }

    rdwr.msgs = msgs;
    rdwr.nmsgs = 1;
    int retry = 0;
    for(retry = 0; retry < max_try;retry++)
    {
        ret = ioctl(fd, I2C_RDWR, &rdwr);
        if (ret < 0) {
            fprintf(stderr, "Error(%d/%d): Sending messages failed: %s\n", retry + 1, max_try, strerror(errno));
        } else if (ret < nmsgs) {
            fprintf(stderr, "Warning: only %d/%d messages were sent\n", ret, nmsgs);
        }else {
            ret = 0;
            break;
        }
        i2c_msleep(5);
    }
#if 0
    printf("i2ctransfer -f -y 11 w%x@0x%x 0x%x", len, dev_addr, reg_addr);
    for(i = 0; i < value_cnt; i++)
    {
        printf(" 0x%x", values[i]);
    }
    printf("\n");
#endif

    return ret;
}

// 打开I2C设备
int i2c_dev_open(struct i2c_dev_info *dev, int i2c_bus, unsigned int dev_addr)
{
    int fd;

    if(dev == NULL)
    {
        goto err;
    }

    i2c_dev_deinit(dev);

    fd = i2c_bus_open(i2c_bus);

    if (fd < 0 || i2c_check_funcs(fd))
    {
        goto err;
    }

    dev->i2c_bus = i2c_bus;
    dev->i2c_fd = fd;
    dev->dev_addr = dev_addr;
    dev->is_init_done = 1;

    return 0;
err:
    dev->is_init_done = 0;
    return -1;
}

int i2c_dev_deinit(struct i2c_dev_info *dev)
{
    int ret;

    if(dev == NULL)
    {
        return -1;
    }
    if(dev->is_init_done != 0)
    {
        ret = close(dev->i2c_fd);
        dev->is_init_done = 0;
    }
    return ret;
}

int i2c_dev_init(struct i2c_dev_info *dev)
{
    if(dev == NULL)
    {
        return -1;
    }
    dev->is_init_done = 0;
    return 0;
}

#define I2C_DEV_FILE_PATH_MAX 64
// 打开I2C设备
int i2c_bus_open(int i2c_bus)
{
    int fd, len;
    char filename[I2C_DEV_FILE_PATH_MAX], size;
    size = I2C_DEV_FILE_PATH_MAX;

    len = snprintf(filename, size, "/dev/i2c/%d", i2c_bus);
    if (len >= (int)size) {
        fprintf(stderr, "%s: path truncated\n", filename);
        return -EOVERFLOW;
    }
    fd = open(filename, O_RDWR);

    if (fd < 0 && (errno == ENOENT || errno == ENOTDIR)) {
        len = snprintf(filename, size, "/dev/i2c-%d", i2c_bus);
        if (len >= (int)size) {
            fprintf(stderr, "%s: path truncated\n", filename);
            return -EOVERFLOW;
        }
        fd = open(filename, O_RDWR);
    }

    if (fd < 0 /*&& !quiet*/) {
        if (errno == ENOENT) {
            fprintf(stderr, "Error: Could not open fd "
                "`/dev/i2c-%d' or `/dev/i2c/%d': %s\n",
                i2c_bus, i2c_bus, strerror(ENOENT));
        } else {
            fprintf(stderr, "Error: Could not open fd "
                "`%s': %s\n", filename, strerror(errno));
            if (errno == EACCES)
                fprintf(stderr, "Run as root?\n");
        }
    }
    if (fd < 0 || i2c_check_funcs(fd))
    {
        return -1;
    }

    return fd;
}

int close_i2c_dev(int fd)
{
    return close(fd);
}

/********************************** for dev api **********************************/
// 对于I2C单一设备的访问，通过 dev api 操作更好。
// 写入8位寄存器中的8位值
int i2c_dev_write_addr8_value8(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_addr8_value8(dev->i2c_fd, dev->dev_addr, reg_addr, reg_value);
}

// 读取8位寄存器中的8位值
int i2c_dev_read_addr8_value8(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char *reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_read_addr8_value8(dev->i2c_fd, dev->dev_addr, reg_addr, reg_value);
}

// 更新写入8位寄存器中的8位值
int i2c_dev_write_update_addr8_value8(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char mask, unsigned char reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_update_addr8_value8(dev->i2c_fd, dev->dev_addr, reg_addr, mask, reg_value);
}

// 写入8位寄存器中的16位值
int i2c_dev_write_addr8_value16(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned short reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_addr8_value16(dev->i2c_fd, dev->dev_addr, reg_addr, reg_value);
}

// 读取8位寄存器中的16位值
int i2c_dev_read_addr8_value16(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned short *reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_read_addr8_value16(dev->i2c_fd, dev->dev_addr, reg_addr, reg_value);
}

// 更新写入8位寄存器中的16位值
int i2c_dev_write_update_addr8_value16(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned short mask, unsigned short reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_update_addr8_value16(dev->i2c_fd, dev->dev_addr, reg_addr, mask, reg_value);
}


// 写入16位寄存器中8位的值
int i2c_dev_write_addr16_value8(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned char reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_addr16_value8(dev->i2c_fd, dev->dev_addr, reg_addr, reg_value);
}

// 读取16位寄存器中8位的值
unsigned char i2c_dev_read_addr16_value8(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned char *reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_read_addr16_value8(dev->i2c_fd, dev->dev_addr, reg_addr, reg_value);
}

// 更新写入16位寄存器中的8位值
int i2c_dev_write_update_addr16_value8(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned char mask, unsigned char reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_update_addr16_value8(dev->i2c_fd, dev->dev_addr, reg_addr, mask, reg_value);
}

// 写入16位寄存器16位值
int i2c_dev_write_addr16_value16(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned short reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_addr16_value16(dev->i2c_fd, dev->dev_addr, reg_addr, reg_value);
}

// 读取16位寄存器中的16位值（端序未确定）
unsigned short i2c_dev_read_addr16_value16(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned short *reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_read_addr16_value16(dev->i2c_fd, dev->dev_addr, reg_addr, reg_value);
}

// 更新写入16位寄存器中的16位值
int i2c_dev_write_update_addr16_value16(struct i2c_dev_info *dev, unsigned short reg_addr, unsigned short mask, unsigned short reg_value)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_update_addr16_value16(dev->i2c_fd, dev->dev_addr, reg_addr, mask, reg_value);
}


// I2C总线连续收
int i2c_dev_read_addr8_valuex(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char *values, unsigned int read_cnt)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_read_addr8_valuex(dev->i2c_fd, dev->dev_addr, reg_addr, values, read_cnt);
}

// for i2c-switcher
int i2c_dev_write_addr8_value0(struct i2c_dev_info *dev, unsigned char reg_addr)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_addr8_value0(dev->i2c_fd, dev->dev_addr, reg_addr);
}

// 针对i2c总线 连续写
int i2c_dev_write_addr8_valuex(struct i2c_dev_info *dev, unsigned char reg_addr, unsigned char *values, unsigned int value_cnt)
{
    if(dev == NULL)
    {
        return -1;
    }
    return i2c_bus_write_addr8_valuex(dev->i2c_fd, dev->dev_addr, reg_addr, values, value_cnt);
}


#if 1
#include <stdio.h>
int test_i2c(void)
{
    unsigned char debug_buf[8] = {0x11, 0x22,0x33,0x44,0x55, 0x66,0x77,0x88};
    struct i2c_dev_info test_dev = {0};
    int i2c_bus,  fd, i;
    int dev_addr = 0x29;
    int ret;
    unsigned short val;

    i2c_bus = 11;

    printf("Using i2c-bus [%d]\n", i2c_bus);

#if 1
    /* for dev api */
    ret = i2c_dev_init(&test_dev);
    printf("i2c_dev_init : ret %d\n", ret);

    ret = i2c_dev_open(&test_dev, i2c_bus, dev_addr);
    printf("i2c_dev_open : ret %d\n", ret);
    if(ret < 0)
    {
        printf("error when dev open\n");
        return -1;
    }

    // test 1
    ret = i2c_dev_write_addr8_value0(&test_dev, 0x02);

    // test 2
    ret = i2c_dev_write_addr8_value8(&test_dev, 0x00, 0xab);
    printf("i2c_dev_write_addr8_value8 : ret %d\n", ret);
    ret = i2c_dev_read_addr8_value8(&test_dev, 0x2d, (unsigned char *)&val);
    printf("i2c_dev_read_addr8_value8 : ret %d\n", ret);

    // test 3 ； 16位寄存器地址，8位值
    ret = i2c_dev_write_addr16_value8(&test_dev, 0x2dff, 0xff);
    printf("i2c_dev_write_addr16_value8 : ret %d\n", ret);
    ret = i2c_dev_read_addr16_value8(&test_dev, 0x2dff, (unsigned char *)&val);
    printf("i2c_dev_read_addr16_value8 : ret %d\n", ret);

    // test 4 ； 16位寄存器地址，16位值
    ret = i2c_dev_write_addr16_value16(&test_dev, 0x2d11, 0xffff);
    printf("i2c_dev_write_addr16_value16 : ret %d\n", ret);
    ret = i2c_dev_read_addr16_value16(&test_dev, 0x2d11, (unsigned short *)&val);
    printf("i2c_dev_read_addr16_value16 : ret %d\n", ret);

    // test 5 连续读写(从寄存器0x00开始)
    ret = i2c_dev_write_addr8_valuex(&test_dev, 0x00, debug_buf, sizeof(debug_buf));
    printf("i2c_dev_write_addr8_valuex : ret %d\n", ret);
    ret = i2c_dev_read_addr8_valuex(&test_dev, 0x00, debug_buf, 8);
    printf("i2c_dev_read_addr8_valuex : ret %d\n", ret);
    for (i = 0; i < ret; i++)
        printf("read--[%d]/[%x]\n",i,debug_buf[i]);

    i2c_dev_deinit(&test_dev);
#endif


#if 1
    /* for bus api */
    fd = i2c_bus_open(i2c_bus);
    if(fd < 0)
    {
        printf("error when bus open\n");
        return -1;
    }

    // test 1
    ret = i2c_bus_write_addr8_value0(fd, dev_addr, 0x02);

    // test 2
    ret = i2c_bus_write_addr8_value8(fd, dev_addr, 0x00, 0xab);
    printf("i2c_bus_write_addr8_value8 : ret %d\n", ret);
    ret = i2c_bus_read_addr8_value8(fd, dev_addr, 0x2d, (unsigned char *)&val);
    printf("i2c_bus_read_addr8_value8 : ret %d\n", ret);

    // test 3 ； 16位寄存器地址，8位值
    ret = i2c_bus_write_addr16_value8(fd, dev_addr, 0x2dff, 0xff);
    printf("i2c_bus_write_addr16_value8 : ret %d\n", ret);
    ret = i2c_bus_read_addr16_value8(fd, dev_addr, 0x2dff, (unsigned char *)&val);
    printf("i2c_bus_read_addr16_value8 : ret %d\n", ret);

    // test 4 ； 16位寄存器地址，16位值
    ret = i2c_bus_write_addr16_value16(fd, dev_addr, 0x2d11, 0xffff);
    printf("i2c_bus_write_addr16_value16 : ret %d\n", ret);
    ret = i2c_bus_read_addr16_value16(fd, dev_addr, 0x2d11, (unsigned short *)&val);
    printf("i2c_bus_read_addr16_value16 : ret %d\n", ret);

    // test 5 连续读写(从寄存器0x00开始)
    ret = i2c_bus_write_addr8_valuex(fd, dev_addr, debug_buf[0], debug_buf + 1, sizeof(debug_buf));
    printf("i2c_bus_write_addr8_valuex : ret %d\n", ret);
    ret = i2c_bus_read_addr8_valuex(fd, 0x20, 0x00, debug_buf, 8);
    printf("i2c_bus_read_addr8_valuex : ret %d\n", ret);
    for (i = 0; i < ret; i++)
        printf("read--[%d]/[%x]\n",i,debug_buf[i]);

#endif

    return 0;
}

#endif
