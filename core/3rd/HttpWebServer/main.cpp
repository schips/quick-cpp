/*================================================================
* Filename:main.cpp
* Author: KCN_yu
* Createtime:Sat 19 Jun 2021 02:57:51 PM CST
================================================================*/

#include "web_server.h"

int main()
{
    int port = 9999;
    const char *work_dir = "/tmp";
    WebServer serv(port);

    if (serv.SetWorkDir(work_dir) != 0)
    {
        printf("Set Dir filed\n");
    }

    serv.RunServer();
    return 0;
}
