#ifndef __FORK_AND_EXEC__
#define __FORK_AND_EXEC__

enum wait_pid_type
{
    WAIT_PID_TYPE_NOWAIT, // 不等待
    WAIT_PID_TYPE_WAIT, // 等待
};

// 执行命令，返回PID
// argv : 一组以NULL结尾的命令行
// is_wait_pid : 如果为0时，不等待结果。
int do_exec_with_args(char **argv, enum wait_pid_type is_wait_pid);

#endif/*__FORK_AND_EXEC__*/
