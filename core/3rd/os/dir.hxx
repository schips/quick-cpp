
#include <unistd.h>
#include <fcntl.h>           /* Definition of AT_* constants */
#include <unistd.h>

#ifdef __cplusplus
extern "C"{
#endif
#define MAX_DIR_LEN 256

//检查目录/文件(所有类型是否存在
// path ： 需要判断的路径
// 返回值：
//   存在则返回0
int IsPathExist(const char* path);


//获取程序所在目录
// buff ： 需要接收的buffer
// maxWrite ： 需要接收的buffer的最大长度
// 返回值：
//   参数错误时时返回-1；成功时返回路径长度（限制于maxWrite）
int getCurrentPath(char * buff, int maxWrite);

// 删除文件（不能删除目录）
void delFile(const char* filename);


#ifdef __cplusplus
}
#endif

/* 如果超出大小(MB)，则删除其中的第一个文件 */
int rmOldestFileIfExceedSize(const char *dir, const char * dev_id, int max_mb);

class CheckSpace {
public:

    // 构造函数
    CheckSpace(const char * filepath = NULL);
    // 更新容量
    void DoCheck(void);

    //获取各项属性
    int GetTB(void);
    int GetGB(void);
    int GetMB(void);
    int GetKB(void);
    int GetBytes(void);

    //展示内容
    void Display(void);

    //Byte数量增加（自动进位）
    void AddBytes(int bytes);
    void AddKBytes(int bytes);
    void AddMBytes(int bytes);
    void AddGBytes(int bytes);

    // 判断容量是否超过指定MB
    int isBiggerThanXMB(int bytes_for_mb, int bytes_for_kb = 0);
private:
    int m_TBytes; // TB
    int m_GBytes; // GB
    int m_MBytes; // MB
    int m_KBytes; // KB
    int m_Bytes; //Byte
    char m_FilePath[128]; //目录地址
    //查看某目录所占空间大小（不含该目录本身的4096Byte）
    void _Check(const char * dir);
};

