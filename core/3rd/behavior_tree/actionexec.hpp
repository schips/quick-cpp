#ifndef ACTIONEXEC_H
#define ACTIONEXEC_H

#include "returncode.hpp"

class ActionExec {
private:
    int counter;
public:
    ActionExec(){ counter = 0; };
    ~ActionExec(){};

    ReturnCode run();

    int getCounter() { return counter; }
};

#endif // ACTIONEXEC_H
