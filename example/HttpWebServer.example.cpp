#include <stdio.h>
#include "web_server.h"

int test_http_web_server(void)
{
    int port = 9999;
    const char *work_dir = "/tmp";
    WebServer serv(port);

    if (serv.SetWorkDir(work_dir) != 0)
    {
        printf("Set Dir filed\n");
    }

    serv.RunServer();
    return 0;
}

#if 0
int main(int argc, char * argv[])
{
    return test_http_web_server();
}
#endif
