#ifdef __cplusplus
extern "C"{
#endif
// C code...

#include <errno.h>
#include <assert.h>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/shm.h>


#include <sys/epoll.h>

#define MAX_EVENT_NUMBER 1024
#define TCP_BUFFER_SIZE 512
#define UDP_BUFFER_SIZE 2048
const char* gip = "0.0.0.0";
int gport = 888;
#define TCP_CLIENT_MAX 64


unsigned char buf_for_send[UDP_BUFFER_SIZE];
unsigned char buf_for_recv[UDP_BUFFER_SIZE];

int tcp_clients[TCP_CLIENT_MAX];
unsigned int tcp_clients_cnt;

static int setnonblocking(int fd)
{
    int old_option = fcntl(fd, F_GETFL);
    int new_option = old_option | O_NONBLOCK;
    fcntl(fd, F_SETFL, new_option);
    return old_option;
}

static void addfd(int epollfd, int fd)
{
    struct epoll_event event;
    event.data.fd = fd;
    event.events = EPOLLIN | EPOLLET;
    epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &event);
    setnonblocking(fd);
}

int add_tcp_client(/*char type,*/ int fd)
{
    int i;
    int *tcp_client;
    unsigned int *tcp_client_cnt;

    tcp_client = tcp_clients;
    tcp_client_cnt = &tcp_clients_cnt;

    if(fd < 0)
    {
        return -1;
    }

    if(tcp_client_cnt[0] >= TCP_CLIENT_MAX)
    {
        return -1;
    }

    for(i =0; i < TCP_CLIENT_MAX; i++)
    {
        if(tcp_client[i] <= 0)
        {
            tcp_client[i] = fd;
            tcp_client_cnt[0] ++;
            return 0;
        }
    }
    return -1;
}

int del_tcp_client(int fd)
{
    int i;
    int *tcp_client;
    unsigned int *tcp_client_cnt;

    tcp_client = tcp_clients;
    tcp_client_cnt = &tcp_clients_cnt;

    for(i =0; i < TCP_CLIENT_MAX; i++)
    {
        if(tcp_client[i] == fd)
        {
            tcp_client[i] = -1;
            tcp_client_cnt --;
            return 0;
        }
    }
    return -1;
}

int creat_tcp_server(int *fd, int port)
{
    int tcp_listenfd, one = 1;
    int ret = 0;

    struct sockaddr_in address;

    bzero(&address, sizeof(address));
    address.sin_family = AF_INET;
    inet_pton(AF_INET, gip, &address.sin_addr);
    address.sin_port = htons(port);

    tcp_listenfd = *fd;

    //msleep(100);
    if(tcp_listenfd != -1)
    {
        close(tcp_listenfd);
    }
    //创建tcp socket ， 并将其绑定到端口port上
    tcp_listenfd = socket(PF_INET, SOCK_STREAM, 0);

    if (setsockopt(tcp_listenfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) < 0) {
        close(tcp_listenfd);
        return -1;
    }

    ret = bind(tcp_listenfd, (struct sockaddr*)&address, sizeof(address));
    if(ret != 0)
    {
        close(tcp_listenfd);
        return -1;
    }

    ret = listen(tcp_listenfd, 50);

    *fd = tcp_listenfd;
    return ret;
}

int creat_udp_server(int *fd, int port)
{
    int udpfd, one = 1;
    int ret = 0;

    struct sockaddr_in address;

    udpfd = *fd;

    if(udpfd != -1)
    {
        close(udpfd);
    }

    //创建udp socket ， 并将其绑定到端口port上
    bzero(&address, sizeof(address));
    address.sin_family = AF_INET;
    inet_pton(AF_INET, gip, &address.sin_addr);
    address.sin_port = htons(port);

    udpfd = socket(PF_INET, SOCK_DGRAM, 0);

    if (setsockopt(udpfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) < 0) {
        close(udpfd);
        return -1;
    }

    ret = bind(udpfd, (struct sockaddr*)&address, sizeof(address));
    if(ret == -1)
    {
        close(udpfd);
        return -1;
    }

    *fd = udpfd;
    return ret;
}

int main(void)
{
    int tcp_listenfd = -1;
    int udpfd = -1;
    int recv_len;

    struct epoll_event events[MAX_EVENT_NUMBER];
    int ret = 0;
    int epollfd = -1;

init:
    creat_tcp_server(&tcp_listenfd, gport);

    creat_udp_server(&udpfd, gport);

    if(epollfd != -1)
    {
        close(epollfd);
    }
    epollfd = epoll_create(50);
    if(epollfd == -1)
    {
        perror("epoll_create"); // assert(epollfd != -1);
        goto init;
    }

    //注册tcp socket和udp socket上的可读事件
    addfd(epollfd, tcp_listenfd);
    addfd(epollfd, udpfd);

    printf("run in %s:%d\n", gip, gport);

    struct sockaddr_in client_address;
    socklen_t client_addrlength = sizeof(client_address);
    while(1)
    {
        printf("Waiting\n\n");
        int number = epoll_wait(epollfd, events, MAX_EVENT_NUMBER, -1);
        if(number < 0)
        {
            if((errno == EINTR) || (errno == EAGAIN) || (errno == EWOULDBLOCK))
            {
                continue;
            }
            perror("epoll failure ");
            goto init;
        }

        for(int i = 0; i < number; ++i)
        {
            int sockfd = events[i].data.fd;
            // tcp 新连接
            if(sockfd == tcp_listenfd)
            {
                int connfd = accept(tcp_listenfd, (struct sockaddr*)&client_address, &client_addrlength);
                addfd(epollfd, connfd);
                add_tcp_client(connfd);
            }
            // udp 有数据
            else if(sockfd == udpfd)
            {
                memset(buf_for_recv, '\0',UDP_BUFFER_SIZE);
                client_addrlength = sizeof(client_address);
                ret = recvfrom(udpfd, buf_for_recv, 1024, 0, (struct sockaddr*)&client_address, &client_addrlength);
                if(ret <= 0)
                {
                    continue;
                }
                printf("Recv : ");
                for(i = 0; i < ret; i++)
                {
                    printf("[%02x] ", buf_for_recv[i]);
                }
                printf("\n");
                //printf("Echo :[%s]\n\n", buf_for_recv);
                sendto(udpfd, buf_for_recv, ret, 0, (struct sockaddr*)&client_address, client_addrlength);
            }
            // tcp数据
            else if(events[i].events & EPOLLIN)
            {
                while(1)
                {
                    memset(buf_for_recv, '\0', TCP_BUFFER_SIZE);
                    ret = recv(sockfd, &recv_len, sizeof(int), 0);
                    if(ret < 0)
                    {
                        if((errno == EAGAIN) || (errno == EWOULDBLOCK))
                        {
                            break;
                        }
                        close(sockfd);
                        del_tcp_client(sockfd);
                        break;
                    }
                    else if(ret == 0)
                    {
                        close(sockfd);
                        del_tcp_client(sockfd);
                        break;
                    }
                    printf("Recv : ");
                    for(i = 0; i < ret; i++)
                    {
                        printf("[%02x] ", buf_for_recv[i]);
                    }
                    printf("\n");
                    send(sockfd, buf_for_recv, ret, 0);
                    break;
                }
            }
            else
            {
                printf("something else happened\n");
            }
        }
    }
    close(tcp_listenfd);
    close(epollfd);
    return 0;
}
#ifdef __cplusplus
}
#endif
