#ifdef __cplusplus
extern "C"{
#endif
#include <stdio.h>

#include "i2c-rw.h"

#ifdef __cplusplus
}
#endif

int test_i2c(void)
{
    unsigned char debug_buf[8] = {0x11, 0x22,0x33,0x44,0x55, 0x66,0x77,0x88};
    struct i2c_dev_info test_dev = {0};
    int i2c_bus,  fd, i;
    int dev_addr = 0x29;
    int ret;
    unsigned short val;

    i2c_bus = 11;

    printf("Using i2c-bus [%d]\n", i2c_bus);

#if 1
    /* for dev api */
    ret = i2c_dev_init(&test_dev);
    printf("i2c_dev_init : ret %d\n", ret);

    ret = i2c_dev_open(&test_dev, i2c_bus, dev_addr);
    printf("i2c_dev_open : ret %d\n", ret);
    if(ret < 0)
    {
        printf("error when dev open\n");
        return -1;
    }

    // test 1
    ret = i2c_dev_write_addr8_value0(&test_dev, 0x02);

    // test 2
    ret = i2c_dev_write_addr8_value8(&test_dev, 0x00, 0xab);
    printf("i2c_dev_write_addr8_value8 : ret %d\n", ret);
    ret = i2c_dev_read_addr8_value8(&test_dev, 0x2d, (unsigned char *)&val);
    printf("i2c_dev_read_addr8_value8 : ret %d\n", ret);

    // test 3 ； 16位寄存器地址，8位值
    ret = i2c_dev_write_addr16_value8(&test_dev, 0x2dff, 0xff);
    printf("i2c_dev_write_addr16_value8 : ret %d\n", ret);
    ret = i2c_dev_read_addr16_value8(&test_dev, 0x2dff, (unsigned char *)&val);
    printf("i2c_dev_read_addr16_value8 : ret %d\n", ret);

    // test 4 ； 16位寄存器地址，16位值
    ret = i2c_dev_write_addr16_value16(&test_dev, 0x2d11, 0xffff);
    printf("i2c_dev_write_addr16_value16 : ret %d\n", ret);
    ret = i2c_dev_read_addr16_value16(&test_dev, 0x2d11, (unsigned short *)&val);
    printf("i2c_dev_read_addr16_value16 : ret %d\n", ret);

    // test 5 连续读写(从寄存器0x00开始)
    ret = i2c_dev_write_addr8_valuex(&test_dev, 0x00, debug_buf, sizeof(debug_buf));
    printf("i2c_dev_write_addr8_valuex : ret %d\n", ret);
    ret = i2c_dev_read_addr8_valuex(&test_dev, 0x00, debug_buf, 8);
    printf("i2c_dev_read_addr8_valuex : ret %d\n", ret);
    for (i = 0; i < ret; i++)
        printf("read--[%d]/[%x]\n",i,debug_buf[i]);

    i2c_dev_deinit(&test_dev);
#endif


#if 1
    /* for bus api */
    fd = i2c_bus_open(i2c_bus);
    if(fd < 0)
    {
        printf("error when bus open\n");
        return -1;
    }

    // test 1
    ret = i2c_bus_write_addr8_value0(fd, dev_addr, 0x02);

    // test 2
    ret = i2c_bus_write_addr8_value8(fd, dev_addr, 0x00, 0xab);
    printf("i2c_bus_write_addr8_value8 : ret %d\n", ret);
    ret = i2c_bus_read_addr8_value8(fd, dev_addr, 0x2d, (unsigned char *)&val);
    printf("i2c_bus_read_addr8_value8 : ret %d\n", ret);

    // test 3 ； 16位寄存器地址，8位值
    ret = i2c_bus_write_addr16_value8(fd, dev_addr, 0x2dff, 0xff);
    printf("i2c_bus_write_addr16_value8 : ret %d\n", ret);
    ret = i2c_bus_read_addr16_value8(fd, dev_addr, 0x2dff, (unsigned char *)&val);
    printf("i2c_bus_read_addr16_value8 : ret %d\n", ret);

    // test 4 ； 16位寄存器地址，16位值
    ret = i2c_bus_write_addr16_value16(fd, dev_addr, 0x2d11, 0xffff);
    printf("i2c_bus_write_addr16_value16 : ret %d\n", ret);
    ret = i2c_bus_read_addr16_value16(fd, dev_addr, 0x2d11, (unsigned short *)&val);
    printf("i2c_bus_read_addr16_value16 : ret %d\n", ret);

    // test 5 连续读写(从寄存器0x00开始)
    ret = i2c_bus_write_addr8_valuex(fd, dev_addr, debug_buf[0], debug_buf + 1, sizeof(debug_buf));
    printf("i2c_bus_write_addr8_valuex : ret %d\n", ret);
    ret = i2c_bus_read_addr8_valuex(fd, 0x20, 0x00, debug_buf, 8);
    printf("i2c_bus_read_addr8_valuex : ret %d\n", ret);
    for (i = 0; i < ret; i++)
        printf("read--[%d]/[%x]\n",i,debug_buf[i]);

#endif

    return 0;
}

