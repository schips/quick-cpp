#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

using std::vector;
using std::string;
#include <iostream>
using std::ios;
using std::cout;
using std::endl;

#ifdef __cplusplus
extern "C"{
#endif

// C code...
#include "ini.h"
#include <string.h>

#ifdef __cplusplus
}
#endif

#define BUILD_IN_INI_SECTION_NAME "GLOBAL"
#define CONFIG_BUFF_MAX_SIZE 64

struct global_setting
{
    int timeout_s;
    string bus;
    string file;
    string bus_arg;
};
struct global_setting g_setting;

struct configuration
{
    int send_buff_cnt;
    unsigned char send_buff[CONFIG_BUFF_MAX_SIZE];
    int recv_buff_cnt;
    unsigned char recv_buff[CONFIG_BUFF_MAX_SIZE];
} ;


//创建并初始化一个 unordered_map 容器，其存储的 <string, struct configuration> 类型的键值对
std::unordered_map<std::string, struct configuration> my_uMap;

static int cfg_handler(void* user, const char* section, const char* name,
                   const char* value)
{
    //struct configuration* pconfig = (struct configuration*)user;
    string temp_str;
    int ret;
    unsigned char *p;
    int *pi;

    #define MATCH(s, n) strcmp(section, s) == 0 && strcmp(name, n) == 0
    #define MATCH_SECTION(s) strcmp(section, s) == 0
    #define MATCH_NAME(s) strcmp(name, s) == 0

    // 这个inih 做了ini 头、键值对的判断、赋值。

    // 全局配置
    if (MATCH_SECTION(BUILD_IN_INI_SECTION_NAME))
    {
        if(MATCH_NAME("bus"))
        {
            g_setting.bus = value;
        }
        if(MATCH_NAME("file"))
        {
            g_setting.file = value;
        }
        if(MATCH_NAME("bus_arg"))
        {
            g_setting.bus_arg = value;
        }
        if(MATCH_NAME("timeout_s"))
        {
            g_setting.timeout_s = atoi(value);
        }
        //printf("Par : [%s] [%s] [%s]\n", section, name, value);
        //pconfig->ip_addr = strdup(value);
    }
    // 命令操作
    else
    {
        //printf("Par : [%s] [%s]\n", name, value);

        if(MATCH_NAME("send_buff"))
        {
            p = my_uMap[section].send_buff;
            pi = &my_uMap[section].send_buff_cnt;
        } else if(MATCH_NAME("recv_buff"))
        {
            p = my_uMap[section].recv_buff;
            pi = &my_uMap[section].recv_buff_cnt;
        }else
        {
            printf("Skip\n");
            return 0;
        }
        temp_str = string(value);
        temp_str = subreplace(temp_str, "'", "");

        ret = str_to_byte(temp_str, p, CONFIG_BUFF_MAX_SIZE);
        *pi = ret;

        for(int i = 0; i < ret ; i++)
        {
            printf(" 0x%02x  ", p[i]);
        }
        printf("\n");
        printf("\n");
    }
    return 1;
}

int load_config(const char * ini_file)
{
    if(ini_file == NULL)
    {
        return -1;
    }

    //struct configuration config;

    //if (ini_parse(ini_file, cfg_handler, &config) < 0)
    if (ini_parse(ini_file, cfg_handler, NULL) < 0)
    {
        printf("Can't load '%s'\n", ini_file);
        return -1;
    }
    return 0;
}
#if 0
[GLOBAL]
bus = uart
file = /dev/ttyS3
bus_arg = 115200
timeout=1


; 保存当前配置
[save_config]
send_buff = 'AA 04 00 11 01 C0 EB AA'
recv_buff = '55 05 00 11 33 01 9F EB AA'

; 恢复出厂设置
[reset_as_factory]
send_buff = 'AA 04 00 12 02 C2 EB AA'
recv_buff = '55 05 00 12 33 01 A0 EB AA'

#endif

int main(int argc, char* argv[])
{
    int ret;
    string ini_file("test.ini");
    string action;
    char *tmp;
    unsigned char *send_buff, *recv_buff;
    int send_buff_cnt, recv_buff_cnt;

    // 参数1 ： 指定发送的串口命令
    if( argc >= 2 )
    {
        action = argv[1];
    }
    if(action == BUILD_IN_INI_SECTION_NAME)
    {
        printf("[%s] is NOT FOR USE.\n", action.c_str());
        return -1;
    }

    ret = load_config(ini_file.c_str());

    return ret;
}

