#include <iosteam>
/* cmd parser*/
#include "clipp.hxx"
using namespace clipp;

int parseCarg(int argc, char *argv[],
              string& run_mode, string& cmd, string& set_value, string& address)
{
    string addr;
    auto appCli = (option("-m", "--mode") & value("run mode", run_mode),
                option("-c", "--module") & value("module cmd", cmd),
                option("-v", "--value") & value("cmd value", set_value),
                option("-a", "--address") & value("set ip:port", addr));
    if (!parse(argc, const_cast<char **>(argv), appCli)) {
        std::cerr << make_man_page(appCli, argv[0]) << std::endl;
        return 1;
    }
    if(addr != "")
        address = addr;

    return 0;
}

#if 0
命令要求 格式要求
     -m run/get/set -c module-name [set-value, 'ok' or other]
 例如
     -m run  -c demo
     -m get  -c demo
     -m set  -c demo -v no
     -m get  -c demo
     -m set  -c demo -v ok
     -m get  -c demo
#endif
int test_clipp(void)
{
    std::string run_mode, cmd, set_value;
    string addr;

    // 如果有参数传递，则解析
    if(argc > 1)
    {
        //bool usingConfig = false;
        int ret;
        // 解析 : 模块动作， 模块参数， 参数值， [服务器地址]
        ret = parseCarg(argc, argv, run_mode, cmd, set_value, addr);
        if(ret) return ret;
        LogD(__FILE__, __LINE__, "run mode", run_mode);
        LogD(__FILE__, __LINE__, "cmd", cmd);
        if(addr != "")
            socketTrySetaddress(addr);

        if(run_mode == "")
        {
            //ret = runAsServer();
        }else {
            //ret = runAsClient(run_mode, cmd, set_value);
        }
        return ret;
    }

    return 0;
}
