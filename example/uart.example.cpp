#ifdef __cplusplus
extern "C"{
#endif
#include <stdio.h>
#include "uart.h"
#ifdef __cplusplus
}
#endif

int serial_init(char * uart_port, int speed)
{
    int fd;
    int SerialSpeed = speed;

    printf("Open [%s] with [%d]\n", uart_port, speed);

    fd = serial_open(uart_port);
    if(fd == -1)
    {
        printf("open fail\n");
        return -1; // 失败
    }
    if (serial_set(fd, SerialSpeed, 0, 8, 1, 'N') != 0)
    {
        return -1;
    }
    return fd;
}

int uart_demo(char * uart_dev_path)
{
    // 打开串口
    unsigned int i;
    char uart_path [32] = "/dev/ttyS6";
    int speed = 115200;
    int uart_fd;

    if(uart_dev_path != NULL)
    {
        uart_fd = serial_init(uart_dev_path, speed);
    } else
    {
        uart_fd = serial_init(uart_path, speed);
    }
    if(uart_fd < 0)
    {
        return -1;
    }

    // 接收uart
    unsigned char recv_buff[256];
    int timeout_s = 0;
    int timeout_us = 3000;
    int len;

    printf("Start Echo.\n");
    while(1)
    {
        len = serial_recv(uart_fd, recv_buff, sizeof(recv_buff), timeout_s, timeout_us);
        if (len <= 0)
        {
            continue;
        }
        // 将收到的数据发送出去
        serial_send(uart_fd, recv_buff, len);
    }

    return 0;
}

/*
int main(int argc, char * argv[])
{
    if(argc != 0)
    {
        return uart_demo(argv[1]);
    }
    return 0;
}
*/
