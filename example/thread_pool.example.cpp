#include "thread_pool.hxx"

// 线程函数
static void thread_test(thread_context_t *thread_ctx)
{
    char * pdata = (char *)getTaskArgFromContext(thread_ctx);

    if(pdata)
    {
        printf("Data %p\n", pdata);
    }
}

static class threadPool *m_pool;

int threadpool_demo(void)
{
    int thread_count = 20;
    int queue_size = 64;

    char data1[256];

    m_pool = new class threadPool(thread_count, queue_size);

    m_pool->addTask(thread_test, data1);

    //char data2[256];
    //m_pool->addTask(thread_test, data2);

    while(1);

    delete m_pool;

    return 0;
}

/*
int main(int argc, char * argv[])
{
    threadpool_demo();
    return 0;
}
*/

