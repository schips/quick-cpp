#if 1
#include <iostream>
//using namespace std;
using std::cout;
using std::endl;
#include <string>
#include <random>


#include "node.hpp"
#include "returncode.hpp"

#include "actionexec.hpp"

//#include "log.hpp"
// https://stackoverflow.com/questions/5028302/small-logger-class


ActionExec exec;


ReturnCode succ_fail()
{
    random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    uniform_int_distribution<> dis(0,1);
    int true_false = dis(gen);
    if (true_false==1)
    {
        std::cout << "---> success"  << std::endl;
        return ReturnCode::SUCCESS;
    }
    else
    {
        std::cout << "---> failure"  << std::endl;
        return ReturnCode::FAILURE;
    }
}

ReturnCode succ_runn()
{
    random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    uniform_int_distribution<> dis(0,1);
    int true_false = dis(gen);
    if (true_false == 1)
    {
        std::cout << "---> success"  << std::endl;
        return ReturnCode::SUCCESS;
    }
    else
    {
        std::cout << "---> running"  << std::endl;
        return ReturnCode::RUNNING;
    }
}


ReturnCode predOnComingCar()
{

    std::cout << "predOnComingCar:" << std::endl;

    return succ_fail();

}

ReturnCode predCarInFront()
{
    std::cout << "predCarInFront:"  << std::endl;
    return succ_fail();

}


ReturnCode actionAvoid()
{
    std::cout << "Avoiding car:" << std::endl;
    return exec.run();
    // return succ_runn();
}


ReturnCode actionTurnOut()
{
    std::cout << "Turning out:" << std::endl;
    return exec.run();
    // return succ_runn();
}

ReturnCode actionPassCar()
{
    std::cout << "Passing car:" << std::endl;
    return exec.run();
    // return succ_runn();
}

ReturnCode actionTurnIn()
{
    std::cout << "Turning in:" << std::endl;
    return exec.run();
    // return succ_runn();
}

ReturnCode actionCruise()
{
    std::cout << "Cruising:" << std::endl;
    return exec.run();
    // return succ_runn();
}


static int test_behavior_tree(void)
{

    Selector *sel = new Selector();

    Sequence *seq_oncoming_car = new Sequence();
    sel->addChild(seq_oncoming_car);

    Conditional *cond_oncoming = new Conditional(predOnComingCar);
    seq_oncoming_car->addChild(cond_oncoming);

    Action *act_avoiding = new Action(actionAvoid);
    seq_oncoming_car->addChild(act_avoiding);

    Sequence *seq_overtake = new Sequence();
    sel->addChild(seq_overtake);

    Conditional *cond_car_infront = new Conditional(predCarInFront);
    seq_overtake->addChild(cond_car_infront);

    Sequence *seq_overtake_exec = new Sequence();
    seq_overtake->addChild(seq_overtake_exec);

    Action *act_turn_out = new Action(actionTurnOut);
    seq_overtake_exec->addChild(act_turn_out);

    Action *act_pass_car = new Action(actionPassCar);
    seq_overtake_exec->addChild(act_pass_car);

    Action *act_turn_in = new Action(actionTurnIn);
    seq_overtake_exec->addChild(act_turn_in);

    Action *act_cruise = new Action(actionCruise);
    sel->addChild(act_cruise);


    ReturnCode result;
    result = sel->tick();
    // while (result != ReturnCode::SUCCESS)
    for (int i = 0; i < 15; ++i)
    {
        std::cout << "count: " << exec.getCounter()  << std::endl;
        std::cout << "---------- new iteration ---------" << std::endl;
        result = sel->tick();
    }

    // Conditional cond (pred);
    // cout << (cond.tick() == ReturnCode::SUCCESS) << endl;


    std::cout << "code working so far"  << std::endl;
    return 0;
}
#endif

