#ifdef __cplusplus
extern "C"{
#endif
#include "ncx_slab.h"
#ifdef __cplusplus
}
#endif

int test_mempoll(int argc, char **argv)
{
    char *p;
    size_t  pool_size = 4096000;  //4M
    ncx_slab_stat_t stat;
    u_char  *space;
    space = (u_char *)malloc(pool_size);

    ncx_slab_pool_t *sp;
    sp = (ncx_slab_pool_t*) space;

    // 指定使用的buffer
    sp->addr = space;
    // 按2的幂次对齐（2^3 = 8 Byte）
    sp->min_shift = 3;
    // 默认写法，连续空间的结尾
    sp->end = space + pool_size;

    // 初始化内存池
    ncx_slab_init(sp);

    // 测试
    int i;
    for (i = 0; i < 1000000; i++)
    {
        // 申请内存
        p = ncx_slab_alloc(sp, 128 + i);
        if (p == NULL)
        {
            printf("%d\n", i);
            return -1;
        }
        // 释放内存
        ncx_slab_free(sp, p);
    }
    // 打印内存池当前状态
    ncx_slab_stat(sp, &stat);

    printf("##########################################################################\n");
    for (i = 0; i < 2500; i++)
    {
        p = ncx_slab_alloc(sp, 30 + i);
        if (p == NULL)
        {
            printf("%d\n", i);
            return -1;
        }

        if (i % 3 == 0)
        {
            ncx_slab_free(sp, p);
        }
    }
    ncx_slab_stat(sp, &stat);

    free(space);

    return 0;
}
