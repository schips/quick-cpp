set(core "./core")
set(m3rd "${core}/3rd")

## 额外的C编译选项
add_c_flag("-Wall -g")
## 额外的C++编译选项
add_cxx_flag("-Wall")

## 额外的链接选项
#add_ld_flag("-lpthread")
#add_ld_flag("-lm")

## 额外的宏定义
#add_define("VERSION=1.0")

## 额外的链接库
add_lib_for_link("pthread")
## 额外的链接目录
#add_lib_path_for_link("./")

## 指定的源文件目录是否加入头文件跟踪
#set (INLUCDE_CODE_DIR ON) # ON or OFF

## 指定的交叉工具链
#set (CROSS_COMPILE "arm-linux-")


add_module("${m3rd}/argv_split")
#add_module("${m3rd}/behavior_tree")
#add_module("${m3rd}/cJSON")
add_module("${m3rd}/clipp")
add_module("${m3rd}/cmd2argv")
add_module("${m3rd}/fifo_map")
#add_module("${m3rd}/HttpWebServer")
#add_module("${m3rd}/i2c")
#add_module("${m3rd}/inih")
add_module("${m3rd}/jsoncpp")
#add_module("${m3rd}/libiop") # BUG: Can not compile pass
add_module("${m3rd}/minilog")
add_module("${m3rd}/mult_timer")
add_module("${m3rd}/my_string")
add_module("${m3rd}/ncx_mempool")
add_module("${m3rd}/os")
add_module("${m3rd}/ring_buffer")
add_module("${m3rd}/rxi_log")
add_module("${m3rd}/threadpool")
#add_module("${m3rd}/uart")
add_module("${m3rd}/value_op")

